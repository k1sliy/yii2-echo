<?php


namespace common\broadcasting\components;

use common\broadcasting\broadcasters\Broadcaster;
use common\broadcasting\events\BroadcastEvent;
use Yii;
use yii\base\Component;
use yii\di\Instance;
use yii\helpers\ArrayHelper;

/**
 * @property-read string|null Socket ID for the current request
 */
class BroadcastManager extends Component
{
    /**
     * @var \common\broadcasting\broadcasters\Broadcaster
     */
    public $broadcaster;


    public function init()
    {
        $this->broadcaster = Instance::ensure($this->broadcaster, Broadcaster::class);
    }


    /**
     * Dispatch event
     *
     * @param \common\broadcasting\events\BroadcastEvent $event
     * @param bool $toOthers
     */
    public function dispatchEvent(BroadcastEvent $event, $toOthers = false)
    {
        $name = $event->broadcastAs();

        $channels = ArrayHelper::toArray($event->broadcastOn());

        $payload = $event->broadcastWith();

        if ($toOthers === true) {
            array_merge($payload, ['socket' => $this->getSocketId()]);
        }

        $this->broadcaster->broadcast($channels, $name, $payload);
    }


    /**
     * Get the socket ID for the current request
     *
     * @return string|null
     */
    public function getSocketId()
    {
        return ArrayHelper::getValue(Yii::$app, 'request.headers.X-Socket-ID');
    }

}

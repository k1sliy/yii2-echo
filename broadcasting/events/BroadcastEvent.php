<?php

namespace common\broadcasting\events;

use ReflectionClass;
use ReflectionProperty;
use yii\base\Object;
use yii\helpers\Inflector;

abstract class BroadcastEvent extends Object
{
    /**
     * Get the channels the event should broadcast on
     *
     * @return string|array
     */
    abstract public function broadcastOn();


    /**
     * The event's broadcast name
     *
     * @return string
     */
    public function broadcastAs()
    {
        return Inflector::camel2id(static::class, '.');
    }


    /**
     * Get the data to broadcast
     *
     * @return array
     */
    public function broadcastWith()
    {
        $class = new ReflectionClass($this);
        $data = [];
        foreach ($class->getProperties(ReflectionProperty::IS_PUBLIC) as $property) {
            if (!$property->isStatic()) {
                $name = $property->getName();
                $data[$name] = $property->getValue($this);
            }
        }

        return $data;
    }

}

<?php

namespace common\broadcasting\controllers;

use common\broadcasting\broadcasters\Broadcaster;
use common\broadcasting\broadcasters\BroadcasterInterface;
use common\broadcasting\components\BroadcastManager;
use Yii;
use yii\di\Instance;
use yii\web\Controller;

class BroadcastingController extends Controller
{
    public $broadcasterComponent = 'broadcastManager';

    public function actionAuth()
    {
        /** @var \common\broadcasting\components\BroadcastManager $broadcastManager */
        $broadcastManager = Instance::ensure($this->broadcasterComponent, BroadcastManager::class);
        $channelName = Yii::$app->request->post('channel_name');

        return $broadcastManager->broadcaster->auth(Yii::$app->user, $channelName);
    }

}

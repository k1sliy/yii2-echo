# Yii2 Echo

### Concept overview
Иплементация API вещания из Laravel для Yii2 для совместимости библиотек.

Как сделано у них https://laravel.com/docs/5.4/broadcasting

Основная суть Echo - предоставить простой API чтобы можно было быстро
внедрять веб сокеты не отвлекаясь на реализацию инфраструктуры.

Вся магия находится в клиентской библиотеке https://github.com/laravel/echo

Данная библеотека имеет несколько коннекторов: https://github.com/laravel/echo/tree/master/src/connector
Основной упор сделан на pusher.com сервис, но так же есть socketio-connector.ts

Реализация socket.io сервера:
https://github.com/tlaverdure/laravel-echo-server


### Установка и запуск
1) Копировать репозиторий в common
2) Убедиться, что включены prettyUrl
3) Создать в веб-приложении контроллер, можно унаследоваться от контроллера
в репозитории, точка входа желательно чтобы была `/broadcasting/auth`,
чтобы ничего править не пришлось
4) Установить laravel-echo-server глобально через Npm
5) Инициализировать laravel-echo-server в корне приложения, это создасть файл
laravel-echo-server.json примерно следующего содержания:
```json
{
	"authHost": "http://signal.dev/",
	"authEndpoint": "/broadcasting/auth",
	"clients": [
		{
			"appId": "01c1a08809bf0557",
			"key": "7d07945f2f86fbcaa7350264fb29245e"
		}
	],
	"database": "redis",
	"databaseConfig": {
		"redis": {},
		"sqlite": {
			"databasePath": "/database/laravel-echo-server.sqlite"
		}
	},
	"devMode": true,
	"host": null,
	"port": "6001",
	"protocol": "http",
	"socketio": {},
	"sslCertPath": "",
	"sslKeyPath": ""
}
```

В данном примере используем redis,
6) Установите redis
7) Установите пакет yii2-redis
8) Запустите сервер выполнив `laravel-echo-server start` в корне приложения

Фронтенд:
1) В репозитории есть echo.js - собранная библиотека, подключите её через ассеты или script
2) Нужно подключить socket.io клиентскую библиотеку, laravel-echo-server может её расшарить, подробнее:
https://github.com/tlaverdure/laravel-echo-server#socketio-client-library
Либо скачайте сами.
3) Пишем логику:
```js
window.io = require('socket.io-client'); // В данном примере библиотеку подтянули из npm
window.Echo = new Echo({
    broadcaster: 'socket.io',
    host: window.location.hostname + ':6001'
});
// Подписываемся на приватный канал signal
window.Echo.private('signal')
    .listen('.new', (e) => {
        // При событии new делаем магию
        console.log(e);
        document.getElementById('signal-new').play();
        let message = `Новый сигнал по паре ${e.pair}`;
        window.toastr['info'](message, null, {
            "closeButton": true,
            "positionClass": "toast-bottom-right",
            "timeOut": "86400000",
            "extendedTimeOut": "86400000",
        });
    });
```
Внимание, тут шаблонные строки и стрелочные функции!

Бекенд:
Для конфигурации событий я использовал https://github.com/bariew/yii2-event-component
```php
'bootstrap' => [
    \bariew\eventManager\EventBootstrap::class,
],

'components' => [
    'eventManager' => [
        'class' => \bariew\eventManager\EventManager::class,
        'events' => [
            // Подписываемся на событие Signal::EVENT_AFTER_INSERT
            \backend\models\Signal::class => [
                \backend\models\Signal::EVENT_AFTER_INSERT => [
                    function (\yii\base\Event $event) {
                        /** @var \backend\models\Signal $signal */
                        $signal = $event->sender;
                        // Инициализируем собитые SignalNew
                        $event = new \common\models\events\SignalNew([
                            'pair' => $signal->pair->name,
                        ]);
                        // Отправляем его в менеджер событий, окуда он улетит в броадкастер
                        Yii::$app->broadcastManager->dispatchEvent($event);
                    },
                ],
            ],
        ],
    ],
    'broadcastManager' => [
        'class' => \common\broadcasting\components\BroadcastManager::class,
        'broadcaster' => [
            'class' => \common\broadcasting\broadcasters\RedisBroadcaster::class,
            'redis' => [
                'class' => \yii\redis\Connection::class,
            ],
            // Конфигурировать колбек авторизации нужно только для приватных каналов и каналов присутствия
            'channels' => [
                'signal' => function (\yii\web\User $user) {
                    /** @var \common\models\User|null $identity */
                    $identity = $user->identity;
                    return !is_null($identity) && $identity->subscribe_expire_at > time();
                },
            ],
        ],
    ],
],
```

Содержимое SignalNew:
```php
<?php

namespace common\models\events;

use common\broadcasting\channels\PrivateChannel;
use common\broadcasting\events\BroadcastEvent;

class SignalNew extends BroadcastEvent
{
    public $pair;

    public $url;

    public function broadcastOn()
    {
        return new PrivateChannel('signal');
    }


    public function broadcastAs()
    {
        return 'new';
    }

}

```


### TODO
[ ] Тесты
[ ] Документация
[ ] Форнуть laravel-echo и сделать билд, опубликовать. Выкинуть всё лишнее, Laravel зависимое.
[ ] Ассеты для билда Echo, socket.io библиотеки, pusher.com библиотеки
[ ] Pusher.com Broadcaster
[ ] Возможно форкнуть laravel-echo-server и убрать из названия Laravel :)
